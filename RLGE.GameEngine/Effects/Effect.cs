﻿using System.Diagnostics;
using System.IO;
using OpenTK.Graphics.OpenGL4;

namespace RLGE.GameEngine.Effects
{
    public abstract class Effect
    {
        protected abstract string ShaderName { get; }
        private int shaderProgram;

        protected Effect()
        {
            shaderProgram = GL.CreateProgram();

            AddShader(ShaderType.VertexShader);
            AddShader(ShaderType.FragmentShader);
        }

        public virtual void Use()
        {
            GL.UseProgram(shaderProgram);
        }

        public void AddShader(ShaderType type)
        {
            string shaderExtension;
            switch (type)
            {
                case ShaderType.VertexShader:
                    shaderExtension = "vert";
                    break;
                case ShaderType.FragmentShader:
                    shaderExtension = "frag";
                    break;
                case ShaderType.GeometryShader:
                    shaderExtension = "geom";
                    break;
                default:
                    shaderExtension = "shader";
                    break;
            }

            var shaderPath = string.Format("Contents/Shaders/{0}.{1}", ShaderName, shaderExtension);
            var shader = CreateShader(type, shaderPath);

            GL.AttachShader(shaderProgram, shader);
        }

        private static int CreateShader(ShaderType type, string path)
        {
            var shader = GL.CreateShader(type);

            using (var stream = new StreamReader(path))
            {
                GL.ShaderSource(shader, stream.ReadToEnd());
            }

            GL.CompileShader(shader);

            Debug.WriteLine(GL.GetShaderInfoLog(shader));

            return shader;
        }

        public void Link()
        {
            GL.LinkProgram(shaderProgram);

            Debug.WriteLine(GL.GetProgramInfoLog(shaderProgram));
        }

        protected int GetUniformLocation(string name)
        {
            return GL.GetUniformLocation(shaderProgram, name);
        }
    }
}
