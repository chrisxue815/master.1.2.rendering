﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using RLGE.GameEngine.Model;

namespace RLGE.GameEngine.Effects
{
    public class InkWashEffect : Effect
    {
        protected override string ShaderName
        {
            get { return "ink-wash"; }
        }

        private Texture brushTexture;
        private Texture inkTexture;

        private readonly int worldLocation;
        private readonly int viewLocation;
        private readonly int projectionLocation;
        private readonly int directionalLightBaseColorLocation;
        private readonly int directionalLightBaseAmbientIntensityLocation;
        private readonly int directionalLightBaseDiffuseIntensityLocation;
        private readonly int directionalLightDirectionLocation;
        private readonly int cameraPositionLocation;
        private readonly int specularIntensityLocation;
        private readonly int specularPowerLocation;
        private readonly int diffuseTextureEnabledLocation;

        public InkWashEffect()
        {
            AddShader(ShaderType.GeometryShader);
            Link();
            base.Use();

            worldLocation = GetUniformLocation("World");
            viewLocation = GetUniformLocation("View");
            projectionLocation = GetUniformLocation("Projection");
            directionalLightBaseColorLocation = GetUniformLocation("DirectionalLight.Base.Color");
            directionalLightBaseAmbientIntensityLocation = GetUniformLocation("DirectionalLight.Base.AmbientIntensity");
            directionalLightBaseDiffuseIntensityLocation = GetUniformLocation("DirectionalLight.Base.DiffuseIntensity");
            directionalLightDirectionLocation = GetUniformLocation("DirectionalLight.Direction");
            cameraPositionLocation = GetUniformLocation("CameraPosition");
            specularIntensityLocation = GetUniformLocation("SpecularIntensity");
            specularPowerLocation = GetUniformLocation("SpecularPower");
            diffuseTextureEnabledLocation = GetUniformLocation("DiffuseTextureEnabled");

            var diffuseTextureLocation = GetUniformLocation("DiffuseTexture");
            GL.Uniform1(diffuseTextureLocation, 0);

            var brushTextureLocation = GetUniformLocation("BrushTexture");
            GL.Uniform1(brushTextureLocation, 30);

            var inkTextureLocation = GetUniformLocation("InkTexture");
            GL.Uniform1(inkTextureLocation, 31);

            brushTexture = Texture.Load("Contents/Textures/brush.png");
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            inkTexture = Texture.Load("Contents/Textures/ink3.jpg");
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
        }

        public override void Use()
        {
            base.Use();
            if (brushTexture != null)
            {
                brushTexture.Use(TextureUnit.Texture30);
            }
            if (inkTexture != null)
            {
                inkTexture.Use(TextureUnit.Texture31);
            }
        }

        public Matrix4 World
        {
            set
            {
                GL.UniformMatrix4(worldLocation, false, ref value);
            }
        }

        public Matrix4 View
        {
            set
            {
                GL.UniformMatrix4(viewLocation, false, ref value);
            }
        }

        public Matrix4 Projection
        {
            set
            {
                GL.UniformMatrix4(projectionLocation, false, ref value);
            }
        }

        public DirectionalLight DirectionalLight
        {
            set
            {
                GL.Uniform3(directionalLightBaseColorLocation, value.Color);
                GL.Uniform1(directionalLightBaseAmbientIntensityLocation, value.AmbientIntensity);
                GL.Uniform1(directionalLightBaseDiffuseIntensityLocation, value.DiffuseIntensity);
                value.Direction.Normalize();
                GL.Uniform3(directionalLightDirectionLocation, value.Direction);
            }
        }

        public Vector3 CameraPosition
        {
            set
            {
                GL.Uniform3(cameraPositionLocation, value);
            }
        }

        public float SpecularIntensity
        {
            set
            {
                GL.Uniform1(specularIntensityLocation, value);
            }
        }

        public float SpecularPower
        {
            set
            {
                GL.Uniform1(specularPowerLocation, value);
            }
        }

        public bool DiffuseTextureEnabled
        {
            set
            {
                GL.Uniform1(diffuseTextureEnabledLocation, value ? 1 : 0);
            }
        }
    }
}
