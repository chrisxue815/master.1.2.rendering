﻿using OpenTK;

namespace RLGE.GameEngine.Effects
{
    public class LightBase
    {
        public Vector3 Color;
        public float AmbientIntensity;
        public float DiffuseIntensity;
    }

    public class DirectionalLight : LightBase
    {
        public Vector3 Direction;
    }
}
