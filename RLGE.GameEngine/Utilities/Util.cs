﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace RLGE.GameEngine.Utilities
{
    public static class Util
    {
        public static Quaternion FromVectors(ref Vector3 a, ref Vector3 b)
        {
            Vector3 axis;
            Vector3.Cross(ref a, ref b, out axis);
            axis.Normalize();

            float angle;
            Vector3.Dot(ref a, ref b, out angle);
            angle /= a.Length * b.Length;

            return Quaternion.FromAxisAngle(axis, angle);
        }

        public static Quaternion FromVectors(Vector3 a, Vector3 b)
        {
            Vector3 axis;
            Vector3.Cross(ref a, ref b, out axis);
            if (axis.LengthSquared < 0.001f)
            {
                return Quaternion.Identity;
            }
            axis.Normalize();

            float dot;
            Vector3.Dot(ref a, ref b, out dot);
            var angle = (float)Math.Acos(dot / a.Length / b.Length);

            return Quaternion.FromAxisAngle(axis, angle);
        }

        public static void CheckError()
        {
            var err = GL.GetError();
            if (err != 0)
            {
                throw new Exception(err.ToString());
            }
        }
    }
}
