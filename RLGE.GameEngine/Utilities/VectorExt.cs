﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assimp;
using OpenTK;

namespace RLGE.GameEngine.Utilities
{
    public static class VectorExt
    {
        public static Vector2 ToOpenTK(this Vector2D v)
        {
            return new Vector2(v.X, v.Y);
        }

        public static Vector2D ToAssimp(this Vector2 v)
        {
            return new Vector2D(v.X, v.Y);
        }

        public static Vector3 ToOpenTK(this Vector3D v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }

        public static Vector3D ToAssimp(this Vector3 v)
        {
            return new Vector3D(v.X, v.Y, v.Z);
        }

        public static Vector3 Max(Vector3 v, Vector3 v2)
        {
            if (v2.X > v.X) v.X = v2.X;
            if (v2.Y > v.Y) v.Y = v2.Y;
            if (v2.Z > v.Z) v.Z = v2.Z;
            return v;
        }

        public static Vector3 Min(Vector3 v, Vector3 v2)
        {
            if (v2.X < v.X) v.X = v2.X;
            if (v2.Y < v.Y) v.Y = v2.Y;
            if (v2.Z < v.Z) v.Z = v2.Z;
            return v;
        }

        public static Vector3D Max(Vector3D v, Vector3D v2)
        {
            if (v2.X > v.X) v.X = v2.X;
            if (v2.Y > v.Y) v.Y = v2.Y;
            if (v2.Z > v.Z) v.Z = v2.Z;
            return v;
        }

        public static Vector3D Min(Vector3D v, Vector3D v2)
        {
            if (v2.X < v.X) v.X = v2.X;
            if (v2.Y < v.Y) v.Y = v2.Y;
            if (v2.Z < v.Z) v.Z = v2.Z;
            return v;
        }

        public static float MaxDimension(this Vector3 v)
        {
            return v.X > v.Y ? (v.X > v.Z ? v.X : v.Z) : (v.Y > v.Z ? v.Y : v.Z);
        }
    }
}
