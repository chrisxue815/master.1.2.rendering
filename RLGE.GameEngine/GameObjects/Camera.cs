﻿using System;
using OpenTK;
using OpenTK.Input;

namespace RLGE.GameEngine.GameObjects
{
    public class Camera : GameObject
    {
        public Vector3 Position { get; private set; }

        public float FieldOfView { get; set; }
        public float ZNear { get; set; }
        public float ZFar { get; set; }

        public Matrix4 View { get; set; }
        public Matrix4 Projection { get; set; }

        private Vector3 targetPosition;

        private bool following;
        private GameEntity target;
        private Vector3 offset;
        private Vector3 targetCenterOffset;

        private int previousX;
        private int previousY;
        private int previousWheel;

        public Vector3 Right
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(Vector3.UnitY, offset));
            }
        }

        public Vector3 Front
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(Vector3.UnitY, Right));
            }
        }

        public Camera(GameBase game)
            : base(game)
        {
            FieldOfView = MathHelper.PiOver4;
            ZNear = 0.1f;
            ZFar = 1000.0f;
        }

        public override void Initialize()
        {
            if (following && target != null) targetPosition = target.Position + targetCenterOffset;
            View = Matrix4.LookAt(Position, targetPosition, Vector3.UnitY);
        }

        public override void Update()
        {
            base.Update();

            var mouse = Game.Mouse;
            var x = mouse.X;
            var y = mouse.Y;
            var leftPressed = mouse[MouseButton.Left];
            var rightPressed = mouse[MouseButton.Right];
            var wheel = mouse.Wheel;

            var xOffset = x - previousX;
            var yOffset = y - previousY;
            var wheelOffset = wheel - previousWheel;

            if (leftPressed)
            {
                const float rotationSpeed = -0.01f;
                var yAngle = rotationSpeed * yOffset;
                var xAngle = rotationSpeed * xOffset;

                var rotation = Matrix4.CreateRotationY(xAngle);

                var zenith = Math.Acos(Vector3.Dot(offset, Vector3.UnitY) / offset.Length);
                var newZenith = zenith + yAngle;

                if (newZenith > 0 && newZenith < Math.PI)
                {
                    var right = Vector3.Cross(Vector3.UnitY, offset);
                    right.Normalize();

                    rotation *= Matrix4.CreateFromAxisAngle(right, yAngle);
                }

                offset = Vector3.Transform(offset, rotation);
            }

            if (wheelOffset != 0)
            {
                const float zoomingSpeed = 0.5f;
                var offsetDirection = Vector3.Normalize(offset);
                offset -= zoomingSpeed * wheelOffset * offsetDirection;
            }

            if (following && target != null) targetPosition = target.Position + targetCenterOffset;
            Position = targetPosition + offset;
            View = Matrix4.LookAt(Position, targetPosition, Vector3.UnitY);

            previousX = x;
            previousY = y;
            previousWheel = wheel;
        }

        public void UpdateAspectRatio(float aspectRatio)
        {
            Projection = Matrix4.CreatePerspectiveFieldOfView(FieldOfView, aspectRatio, ZNear, ZFar);
        }

        public void UpdateAspectRatio(float width, float height)
        {
            UpdateAspectRatio(width / height);
        }

        public void LookAt(GameEntity target, Vector3 targetCenterOffset, Vector3 cameraOffset)
        {
            following = true;
            this.target = target;
            this.targetCenterOffset = targetCenterOffset;
            offset = cameraOffset;
            Position = target.Position + targetCenterOffset + cameraOffset;
        }

        public void LookAt(Vector3 cameraPosition, Vector3 targetPosition)
        {
            following = false;
            Position = cameraPosition;
            this.targetPosition = targetPosition;
            offset = cameraPosition - targetPosition;
        }

        public void LookAt(Vector3 targetPosition)
        {
            following = false;
            this.targetPosition = targetPosition;
            Position = targetPosition + offset;
        }

        public void Strafe(Vector3 displacement)
        {
            following = false;
            targetPosition += displacement;
            Position += displacement;
        }
    }
}
