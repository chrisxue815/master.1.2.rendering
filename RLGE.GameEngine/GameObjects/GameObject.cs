﻿namespace RLGE.GameEngine.GameObjects
{
    public class GameObject
    {
        protected GameBase Game { get; set; }

        public GameObject(GameBase game)
        {
            Game = game;
        }

        public virtual void Initialize()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }
    }
}
