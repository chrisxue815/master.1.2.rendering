﻿using OpenTK;

namespace RLGE.GameEngine.GameObjects
{
    public class GameEntity : GameObject
    {
        public virtual Vector3 Position { get; set; }
        public Quaternion Orientation;
        public Vector3 Scale;

        public Vector3 InitialRight { get; protected set; }
        public Vector3 InitialUp { get; protected set; }
        public Vector3 InitialFront { get; protected set; }

        public Matrix4 World
        {
            get
            {
                return Matrix4.CreateScale(Scale) * Matrix4.CreateFromQuaternion(Orientation) * Matrix4.CreateTranslation(Position);
            }
        }

        public Vector3 Right
        {
            get
            {
                return Vector3.Transform(InitialRight, Orientation);
            }
        }

        public GameEntity(GameBase game)
            : base(game)
        {
            InitialRight = Vector3.UnitX;
            InitialUp = Vector3.UnitY;
            InitialFront = Vector3.UnitZ;

            Orientation = Quaternion.Identity;
            Scale = new Vector3(1);
        }
    }
}
