﻿using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using RLGE.GameEngine.GameObjects;
using RLGE.GameEngine.Utilities;

namespace RLGE.GameEngine
{
    public class GameBase : GameWindow
    {
        public List<GameObject> GameObjects { get; private set; }
        public Camera Camera;

        public float ElapsedSeconds { get; private set; }
        public float TotalSeconds { get; private set; }

        public bool Running;

        public GameBase()
        {
            GameObjects = new List<GameObject>();

            Camera = new Camera(this);
            GameObjects.Add(Camera);

            Load += (sender, args) => OnLoad();
            UpdateFrame += (sender, args) => OnUpdate(args.Time);
            RenderFrame += (sender, args) => OnDraw(args.Time);
            Resize += (sender, args) => OnResize();

            Running = true;
        }

        private void OnLoad()
        {
            VSync = VSyncMode.On;
            GL.Enable(EnableCap.DepthTest);

            foreach (var gameObject in GameObjects)
            {
                gameObject.Initialize();
            }

            Initialize();
        }

        private void OnResize()
        {
            GL.Viewport(Size);
            Camera.UpdateAspectRatio(Width, Height);
        }

        private void OnUpdate(double time)
        {
            if (Running)
            {
                ElapsedSeconds = (float) time;
                TotalSeconds += ElapsedSeconds;
            }
            else
            {
                ElapsedSeconds = 0;
            }

            Update();

            foreach (var gameObject in GameObjects)
            {
                gameObject.Update();
            }

            Util.CheckError();
        }

        private void OnDraw(double time)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Draw();

            foreach (var gameObject in GameObjects)
            {
                gameObject.Draw();
            }

            SwapBuffers();

            Util.CheckError();
        }

        public virtual void Initialize()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }

        public void SetBorderless()
        {
            WindowBorder = WindowBorder.Hidden;
            //WindowState = WindowState.Maximized;

            //var resolution = Screen.PrimaryScreen.Bounds.Size;
            //Size = new Size(resolution.Width, resolution.Height);
        }
    }
}
