﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Assimp;
using Assimp.Configs;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using RLGE.GameEngine.Utilities;

namespace RLGE.GameEngine.Model
{
    public class Model
    {
        public Scene Scene { get; private set; }
        public Mesh[] Meshes { get; private set; }

        public Vector3 MaxPoint { get; private set; }
        public Vector3 MinPoint { get; private set; }
        public Vector3 BoundingBox { get; private set; }
        public float MaxDimension { get; private set; }
        public float ScaleToUniformCube { get; private set; }

        private Model(Scene scene, string dir)
        {
            Scene = scene;

            var materials = new List<int>[scene.MaterialCount];

            for (var i = 0; i < scene.MaterialCount; i++)
            {
                var material = scene.Materials[i];
                var textures = material.GetTextures(TextureType.Diffuse);
                materials[i] = new List<int>();

                if (textures == null) continue;

                foreach (var textureSlot in textures)
                {
                    var texture = Texture.Load(dir + "/" + textureSlot.FilePath);

                    // We haven't uploaded mipmaps, so disable mipmapping (otherwise the texture will not appear).
                    // On newer video cards, we can use GL.GenerateMipmaps() or GL.Ext.GenerateMipmaps() to create
                    // mipmaps automatically. In that case, use TextureMinFilter.LinearMipmapLinear to enable them.
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                    
                    materials[i].Add(texture.Id);
                }
            }

            Meshes = new Mesh[scene.MeshCount];

            for (var i = 0; i < Meshes.Length; i++)
            {
                var mesh = scene.Meshes[i];
                var textures = materials[mesh.MaterialIndex];
                Meshes[i] = new Mesh(mesh, textures);
            }

            MaxPoint = new Vector3(float.MinValue);
            MinPoint = new Vector3(float.MaxValue);

            foreach (var mesh in Meshes)
            {
                MaxPoint = VectorExt.Max(MaxPoint, mesh.MaxPoint);
                MinPoint = VectorExt.Min(MinPoint, mesh.MinPoint);
            }

            BoundingBox = MaxPoint - MinPoint;
            MaxDimension = BoundingBox.MaxDimension();
            ScaleToUniformCube = 1 / MaxDimension;
        }

        public static Model Load(string path)
        {
            using (var importer = new AssimpImporter())
            {
                var config = new NormalSmoothingAngleConfig(66.0f);
                importer.SetConfig(config);

                var logstream = new LogStream((msg, userData) => Debug.WriteLine(msg));
                importer.AttachLogStream(logstream);

                const PostProcessSteps postProcessSteps =
                    PostProcessSteps.Triangulate |
                    PostProcessSteps.GenerateSmoothNormals |
                    PostProcessSteps.FlipUVs |
                    PostProcessSteps.None;

                var scene = importer.ImportFile(path, postProcessSteps);
                //var scene = importer.ImportFile(path);

                var dir = new DirectoryInfo(path);

                return new Model(scene, dir.Parent.FullName);
            }
        }
    }
}
