﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLGE.GameEngine.Model
{
    public enum EVertexBufferType
    {
        Position,
        Normal,
        TexCoord,
        Index,
        Count
    }

    public struct VertexBufferType
    {
        public static readonly VertexBufferType Position = new VertexBufferType(EVertexBufferType.Position);
        public static readonly VertexBufferType Normal = new VertexBufferType(EVertexBufferType.Normal);
        public static readonly VertexBufferType TexCoord = new VertexBufferType(EVertexBufferType.TexCoord);
        public static readonly VertexBufferType Index = new VertexBufferType(EVertexBufferType.Index);
        public static readonly VertexBufferType Count = new VertexBufferType(EVertexBufferType.Count);

        public EVertexBufferType Value;

        public VertexBufferType(EVertexBufferType value)
        {
            Value = value;
        }

        public static implicit operator int(VertexBufferType a)
        {
            return (int)a.Value;
        }
    }
}
