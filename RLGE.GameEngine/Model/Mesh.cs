﻿using System;
using System.Collections.Generic;
using Assimp;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using RLGE.GameEngine.Utilities;
using PrimitiveType = OpenTK.Graphics.OpenGL4.PrimitiveType;

namespace RLGE.GameEngine.Model
{
    public class Mesh
    {
        public Vector3 MaxPoint { get; private set; }
        public Vector3 MinPoint { get; private set; }
        public Vector3 BoundingBox { get; private set; }

        private readonly int vao;
        private readonly List<int> textures;
        private readonly int numIndices;
        private readonly PrimitiveType primitiveType;

        public Mesh(Assimp.Mesh mesh, List<int> textures)
        {
            this.textures = textures;
            
            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            var buffers = new int[VertexBufferType.Count];
            GL.GenBuffers(buffers.Length, buffers);

            int a = VertexBufferType.Position;
            int b = VertexBufferType.Normal;

            GL.BindBuffer(BufferTarget.ArrayBuffer, buffers[VertexBufferType.Position]);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(12 * mesh.VertexCount), mesh.Vertices, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(VertexBufferType.Position);
            GL.VertexAttribPointer(VertexBufferType.Position, 3, VertexAttribPointerType.Float, false, 0, 0);
            Util.CheckError();

            GL.BindBuffer(BufferTarget.ArrayBuffer, buffers[VertexBufferType.Normal]);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(12 * mesh.Normals.Length), mesh.Normals, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(VertexBufferType.Normal);
            GL.VertexAttribPointer(VertexBufferType.Normal, 3, VertexAttribPointerType.Float, false, 0, 0);
            Util.CheckError();

            //TODO: multiple textures
            var texCoords = mesh.GetTextureCoords(0) ?? new Vector3D[0];
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffers[VertexBufferType.TexCoord]);
            GL.BufferData(BufferTarget.ArrayBuffer, texCoords.Size(), texCoords, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(VertexBufferType.TexCoord);
            GL.VertexAttribPointer(VertexBufferType.TexCoord, 2, VertexAttribPointerType.Float, false, 4, 0);

            var indices = mesh.GetIndices();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, buffers[VertexBufferType.Index]);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Size(), indices, BufferUsageHint.StaticDraw);

            numIndices = indices.Length;
            primitiveType = mesh.OpenGLPrimitiveType();

            GL.BindVertexArray(0);

            var maxPoint = new Vector3D(float.MinValue);
            var minPoint = new Vector3D(float.MaxValue);

            foreach (var vertex in mesh.Vertices)
            {
                maxPoint = VectorExt.Max(maxPoint, vertex);
                minPoint = VectorExt.Min(minPoint, vertex);
            }

            MaxPoint = maxPoint.ToOpenTK();
            MinPoint = minPoint.ToOpenTK();

            BoundingBox = MaxPoint - MinPoint;
        }

        public void Draw()
        {
            GL.BindVertexArray(vao);

            if (textures.Count > 0)
            {
                //TODO: multiple textures
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, textures[0]);
            }

            GL.DrawElements(primitiveType, numIndices, DrawElementsType.UnsignedInt, (IntPtr)0);

            GL.BindVertexArray(0);
        }

        public void DrawVertices()
        {
            GL.BindVertexArray(vao);

            Util.CheckError();
            GL.DrawElements(primitiveType, numIndices, DrawElementsType.UnsignedInt, (IntPtr)0);
            Util.CheckError();

            GL.BindVertexArray(0);
        }
    }
}
