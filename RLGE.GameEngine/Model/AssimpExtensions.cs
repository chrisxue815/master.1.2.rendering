﻿using System;
using Assimp;
using OpenTK;

namespace RLGE.GameEngine.Model
{
    public static class AssimpExtensions
    {
        public static IntPtr Size<T>(this T[] v)
        {
            var singleSize = BlittableValueType<T>.Stride;
            return (IntPtr)(singleSize * v.Length);
        }

        public static int SingleSize<T>(this T[] v)
        {
            return BlittableValueType<T>.Stride;
        }

        public static OpenTK.Graphics.OpenGL4.PrimitiveType OpenGLPrimitiveType(this Assimp.Mesh mesh)
        {
            switch (mesh.PrimitiveType)
            {
                case PrimitiveType.Point:
                    return OpenTK.Graphics.OpenGL4.PrimitiveType.Points;
                case PrimitiveType.Line:
                    return OpenTK.Graphics.OpenGL4.PrimitiveType.Lines;
                case PrimitiveType.Triangle:
                    return OpenTK.Graphics.OpenGL4.PrimitiveType.Triangles;
                case PrimitiveType.Polygon:
                    return OpenTK.Graphics.OpenGL4.PrimitiveType.Quads;
                default:
                    return OpenTK.Graphics.OpenGL4.PrimitiveType.Triangles;
            }
        }
    }
}
