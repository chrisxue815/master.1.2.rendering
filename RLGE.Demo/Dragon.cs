﻿using System.Drawing.Drawing2D;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using RLGE.GameEngine;
using RLGE.GameEngine.Effects;
using RLGE.GameEngine.GameObjects;
using RLGE.GameEngine.Model;
using RLGE.GameEngine.Utilities;

namespace RLGE.Demo
{
    public class Dragon : GameEntity
    {
        public Model model;
        private readonly InkWashEffect effect;

        public Dragon(GameBase game) : base(game)
        {
            model = Model.Load("Contents/Models/horse_hi.obj");
            
            InitialUp = Vector3.UnitY;
            InitialFront = new Vector3(0, -1, 0);

            Orientation = Util.FromVectors(InitialUp, Vector3.UnitY);
            Scale = new Vector3(model.ScaleToUniformCube);

            effect = new InkWashEffect();
            effect.Use();
            
            var directionalLight = new DirectionalLight
            {
                Color = new Vector3(1.0f, 1.0f, 1.0f),
                AmbientIntensity = 0.25f,
                DiffuseIntensity = 0.45f,
                Direction = new Vector3(-1.0f, -1.0f, -1.0f)
            };
            effect.DirectionalLight = directionalLight;

            effect.SpecularIntensity = 10;
            effect.SpecularPower = 100;
            effect.DiffuseTextureEnabled = false;
        }

        public override void Update()
        {
            const float rotationSpeed = -MathHelper.Pi;
            var angle = rotationSpeed * Game.ElapsedSeconds;
            var rotation = Quaternion.FromAxisAngle(Vector3.UnitY, angle);
            Orientation = rotation * Orientation;
        }

        public override void Draw()
        {
            foreach (var mesh in model.Meshes)
            {
                effect.Use();
                effect.World = World;
                effect.View = Game.Camera.View;
                effect.Projection = Game.Camera.Projection;
                effect.CameraPosition = Game.Camera.Position;

                mesh.DrawVertices();
            }
        }
    }
}
