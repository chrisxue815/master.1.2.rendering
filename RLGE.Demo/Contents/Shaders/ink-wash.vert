﻿#version 430

struct SVertexInput
{
  vec3 Position;
  vec3 Normal;
  vec2 TexCoord;
};

in SVertexInput In;

out SVertexOutput
{
  vec3 WorldPos;
  vec3 ToCamera;
  vec3 Normal;
  float VN;
} Out;

uniform mat4 World;
uniform mat4 View;
uniform mat4 Projection;
uniform vec3 CameraPosition;

void main()
{
  vec4 position = vec4(In.Position, 1);
  vec4 normal = vec4(In.Normal, 0);
  
  gl_Position = Projection * View * World * position;
  Out.WorldPos = (World * position).xyz;
  Out.ToCamera = normalize(CameraPosition - Out.WorldPos);
  Out.Normal = (normalize(World * normal)).xyz;
  Out.VN = dot(Out.ToCamera, Out.Normal);
}
