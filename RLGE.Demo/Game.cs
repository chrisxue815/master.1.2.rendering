﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using RLGE.GameEngine;
using RLGE.GameEngine.Model;
using RLGE.GameEngine.Utilities;

namespace RLGE.Demo
{
    public class Game : GameBase
    {
        private bool wasPressingSpace;
        private readonly Dragon dragon;
        private Model[] models = new Model[5];

        public Game()
        {
            Size = new Size(1366, 768);

            dragon = new Dragon(this)
            {
                Position = new Vector3(0, 0, 0)
            };
            GameObjects.Add(dragon);

            Camera.LookAt(dragon, new Vector3(0, 0.2f, 0), new Vector3(0, 1f, 2));

            models[1] = Model.Load("Contents/Models/horse_hi.obj");
            models[2] = Model.Load("Contents/Models/teapot.dae");
            models[3] = Model.Load("Contents/Models/goldfish/goldfish.3ds");
            models[4] = Model.Load("Contents/Models/trex dx/dino videogame.x");
        }

        public override void Initialize()
        {
            base.Initialize();

            GL.ClearColor(Color.Gray);
        }

        public override void Update()
        {
            if (Keyboard[Key.Escape])
            {
                Exit();
            }

            var pressingSpace = Keyboard[Key.Space];
            if (!wasPressingSpace && pressingSpace)
            {
                Running = !Running;
            }

            wasPressingSpace = pressingSpace;

            const float speed = 5;
            
            if (Keyboard[Key.A])
            {
                Camera.Strafe(-speed * ElapsedSeconds * Camera.Right);
            }
            else if (Keyboard[Key.D])
            {
                Camera.Strafe(speed * ElapsedSeconds * Camera.Right);
            }
            else if (Keyboard[Key.W])
            {
                Camera.Strafe(speed * ElapsedSeconds * Camera.Front);
            }
            else if (Keyboard[Key.S])
            {
                Camera.Strafe(-speed * ElapsedSeconds * Camera.Front);
            }
            else if (Keyboard[Key.R])
            {
                Camera.Strafe(speed * ElapsedSeconds * Vector3.UnitY);
            }
            else if (Keyboard[Key.F])
            {
                Camera.Strafe(-speed * ElapsedSeconds * Vector3.UnitY);
            }
            else if (Keyboard[Key.Number1])
            {
                dragon.model = models[1];
                dragon.Orientation = Util.FromVectors(Vector3.UnitY, Vector3.UnitY);
                dragon.Scale = new Vector3(dragon.model.ScaleToUniformCube);
            }
            else if (Keyboard[Key.Number2])
            {
                dragon.model = models[2];
                dragon.Orientation = Util.FromVectors(Vector3.UnitZ, Vector3.UnitY);
                dragon.Scale = new Vector3(dragon.model.ScaleToUniformCube);
            }
            else if (Keyboard[Key.Number3])
            {
                dragon.model = models[3];
                dragon.Orientation = Util.FromVectors(Vector3.UnitY, Vector3.UnitY);
                dragon.Scale = new Vector3(dragon.model.ScaleToUniformCube);
            }
            else if (Keyboard[Key.Number4])
            {
                dragon.model = models[4];
                dragon.Orientation = Util.FromVectors(Vector3.UnitY, Vector3.UnitY);
                dragon.Scale = new Vector3(dragon.model.ScaleToUniformCube);
            }
        }

        public override void Draw()
        {
        }
    }
}
