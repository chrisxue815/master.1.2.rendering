﻿using System;
using System.Diagnostics;
using System.IO;
using RLGE.Demo;

namespace RLGE.Demo
{
    class MyApplication
    {
        [STAThread]
        public static void Main()
        {
            Debug.WriteLine(Directory.GetCurrentDirectory());
            using (var game = new Game())
            {
                // Run the game at 60 updates per second
                game.Run(60.0);
            }
        }
    }
}
